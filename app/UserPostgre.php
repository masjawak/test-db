<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPostgre extends Model
{
    protected $table = 'users';
    protected $connection = 'pgsql';
}
