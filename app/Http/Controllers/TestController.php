<?php

namespace App\Http\Controllers;

use App\User;
use App\UserPostgre;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function mysql(Request $request){
        $start = microtime(true);
        for ($i=0;$i<=10;$i++){
            $user = new User();
            $user->name = 'Name'.$i;
            $user->email = \Str::random(100).'email@local.com';
            $user->password = 'lorem';

            $user->save();
        }
        $time = microtime(true) - $start;
        echo $time.' sec';
    }

    public function postgre(Request $request){
        $start = microtime(true);
        for ($i=0;$i<=10;$i++){
            $user = new UserPostgre();
            $user->name = 'Name '.\Str::random(100);
            $user->email = \Str::random(100).'email@local.com';
            $user->password = 'lorem';

            $user->save();
        }
        $time = microtime(true) - $start;
        echo $time.' sec';
    }
}
